# sort-it-out (srttt)

**sort-it-out (srttt)** is a restructuring program for large piles of images and their companion files. Currently the follwing file formats are supported: JP(E)G, TIF(F), CR2, RAF, HEIC, DNG, XMP. It works non-destructive, i.e. files will be duplicted and not automatically removed. 

**srtt** analyzes a given directory structure containing images and possible companion files (also including library structures of Lightroom, Capture One, Aperture, ...), filters out invisible files and folders, useless metadata, as well as unwanted thumbnails based on a given file size threshold. It flattens the given directory structure so that all files are combined in a single output directory. Files are renamend based on hashing of the original path and filename combined with the filename in clear for identification purposes (e.g. dfb384f974f21d910db1b49e4a27e11daa50c8bd-IMG_1329.HEIC).

USAGE:

    srttt (OPTIONS) <input-path> <output-path>

FLAGS:

    -h, --help       Prints help information

    -V, --version    Prints version information

OPTIONS:

    -t, --threshold <threshold>    Set threshold for thumbnail detection in bytes [default: 600000]

ARGS:

    <input-path>     The directory to read the unsorted images from

    <output-path>    The directory to copy/move the sorted images to