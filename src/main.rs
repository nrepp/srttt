extern crate crypto;
use walkdir::{DirEntry, WalkDir};
use structopt::StructOpt;
use std::path::Path;
use filesize::PathExt;
use std::{fs, io};
use self::crypto::digest::Digest;
use self::crypto::sha1::Sha1;
use std::time::Instant;

/// sort-it-out (srttt) is a restructuring program for large piles of images and their companion files.
/// Currently the follwing file formats are supported: JP(E)G, TIF(F), CR2, RAF, HEIC, DNG, FITS, XMP.
#[derive(StructOpt)]
struct Cli {

    /// The directory to read the unsorted images from
    #[structopt(parse(from_os_str))]
    input_path: std::path::PathBuf,

    /// The directory to copy/move the sorted images to
    #[structopt(parse(from_os_str))]
    output_path: std::path::PathBuf,

    /// Set threshold for thumbnail detection in bytes
    #[structopt(short, long, default_value = "600000")]
    threshold: u64,
   
}

/// Filter for invisible files and directories
fn is_hidden(entry: &DirEntry) -> bool {
    entry.file_name()
         .to_str()
         .map(|s| s.starts_with("."))
         .unwrap_or(false)
}

///Check for thumbnails (i.e. image files smaller than "thumbsize" bytes)
// XMP files are always treated as non-thumbnails
fn is_thumbnail(entry: &Path, thumbsize: u64) -> io::Result<bool> {
    let path = Path::new(entry);
    let metadata = path.symlink_metadata()?;
    let realsize = path.size_on_disk_fast(&metadata)?;
    
    let f_name = path.file_name().unwrap().to_string_lossy();
    
    if !(f_name.ends_with(".XMP") || f_name.ends_with(".xmp")){
        Ok(realsize <= thumbsize)
    }
    else {
        Ok(realsize > thumbsize)
    }
}

///Removes leading "-" and "_" from original filenames
fn f_name_formatter(f_name: String) -> std::string::String{

    let first = f_name[..1].to_string();
    let remainder = f_name[1..].to_string();

    if (first == "_") || (first == "-") {
       return remainder; 
    }
    else {
        return f_name;
    }
}

fn main() -> std::io::Result<()>{
    let starting_time = Instant::now();
    let args = Cli::from_args();

    let walker = WalkDir::new(args.input_path).into_iter();
    let threshold = args.threshold;
    let output_path = args.output_path.to_str().unwrap();
    
    if !(args.output_path.exists()){
        fs::create_dir(output_path)?;
    }

    let mut counter = 0;
    let mut thumbnail_counter = 0;

    for entry in walker.filter_entry(|e| !is_hidden(e)) {
        let entry = entry.unwrap();
      
        let f_name = entry.file_name().to_string_lossy(); 

        if f_name.ends_with(".HEIC") || f_name.ends_with(".heic") ||f_name.ends_with(".CR2") || f_name.ends_with(".cr2") || f_name.ends_with(".RAF") || f_name.ends_with(".raf") || f_name.ends_with(".JPG") || f_name.ends_with(".jpg") || f_name.ends_with(".JPEG") || f_name.ends_with(".jpeg") || f_name.ends_with(".TIF") || f_name.ends_with(".tif") || f_name.ends_with(".TIFF") || f_name.ends_with(".tiff")|| f_name.ends_with(".XMP") || f_name.ends_with(".xmp") || f_name.ends_with(".DNG") || f_name.ends_with(".dng")|| f_name.ends_with(".FITS") || f_name.ends_with(".fits"){    
            
            counter = counter + 1;

            if is_thumbnail(entry.path(), threshold).unwrap() {
                thumbnail_counter = thumbnail_counter + 1;
            }
            else {
                let mut hasher = Sha1::new();   
                hasher.input_str(entry.path().to_str().unwrap());
                let hashed_path = hasher.result_str();
                let f_name_new = format!("{}-{}", hashed_path, f_name_formatter(f_name.to_string()));
                println!("{}", f_name_new);
                let output_path_filename = format!("{}/{}", output_path, f_name_new);
                fs::copy(entry.path().to_str().unwrap(), output_path_filename)?;
            }   
        }      
    }

    println!("+-----------------------------------------------------");
    println!("| Overall image files                        : {}", counter);
    println!("| Thumbnail files (threshold: {} bytes)  : {}", threshold, thumbnail_counter);
    println!("| Sortable image files                       : {}", counter - thumbnail_counter);
    println!("| Elapsed time:                              : {:.2?}", starting_time.elapsed());
    println!("+-----------------------------------------------------");

    Ok(())
}